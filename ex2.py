from __future__ import division

import numpy as np
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import seaborn as sns

data = np.loadtxt('ex2_orig_octave/ex2data1.txt', delimiter=',')
exam_1 = data[:, 0]
exam_2 = data[:, 1]
y = data[:, 2]

M = len(exam_1)

plt.figure()
sns.reset_orig() 
plt.plot(exam_1[y == 0], exam_2[y == 0], 'yo', ms=8, label='Not admitted')
plt.plot(exam_1[y == 1], exam_2[y == 1], 'k+', ms=8, label='Admitted')
sns_reset()
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
plt.xlim(28, 109)
plt.ylim(28, 109)
plt.legend(numpoints=1, loc=1)
plt.savefig('ex2a_plots/fig_1.png', dpi=300)

sigmoid = lambda x: 1 / (1 + np.exp(-x))

X = np.vstack((np.ones(M), exam_1, exam_2)).T

theta_0 = np.zeros(3)

cost_history = [] # 
def cost_function(theta, X, y, M):
    global cost_history
    
    cost = 1 / M * (- y.dot(np.log(sigmoid(theta.dot(X.T)))) - (1 - y).dot(np.log(1 - sigmoid(theta.dot(X.T)))))
    grad = 1 / M * (sigmoid(theta.dot(X.T)) - y).dot(X)

    cost_history.append(cost)
    return cost, grad

res = minimize(cost_function, theta_0, method='L-BFGS-B', args=(X, y, M), jac=True)

theta = res['x']

print 'Cost at best-fit theta: %.3f' % res['fun'] 
print 'Best-fit theta:', ', '.join('%.3f' % item for item in theta)

plt.figure()
plt.scatter(np.arange(len(cost_history)), cost_history, c='k', marker='o')
plt.xlabel('Steps')
plt.ylabel('Cost')
plt.xlim(-res['nit'] * 0.05, res['nit'] * 1.05)
plt.ylim(0, max(cost_history) * 1.05)
plt.savefig('ex2a_plots/cost_vs_steps.png', dpi=300)

num_pts = 10
x_pts, y_pts = np.ogrid[min(exam_1):max(exam_1):num_pts * 1j, min(exam_2):max(exam_2):num_pts * 1j]

plt.figure()
sns.reset_orig()
plt.plot(exam_1[y == 0], exam_2[y == 0], 'yo', ms=8, label='Not admitted')
plt.plot(exam_1[y == 1], exam_2[y == 1], 'k+', ms=8, label='Admitted')
plt.contour(x_pts.ravel(), y_pts.ravel(), theta[0] + theta[1] * x_pts + theta[2] * y_pts - 0.5, levels=[0])
sns_reset()
plt.xlabel('Exam 1 score')
plt.ylabel('Exam 2 score')
plt.xlim(28, 109)
plt.ylim(28, 109)
plt.legend(numpoints=1, loc=1)
plt.savefig('ex2a_plots/fig2.png', dpi=300)

def prediction(theta, exam_1, exam_2):
    """Python version of predict.m."""
    return sigmoid(theta[0] + exam_1 * theta[1] + exam_2 * theta[2])

def accuracy(theta, exam_1, exam_2, y, M):
    return np.sum(np.around(prediction(theta, exam_1, exam_2)) == y) / M

print 'For a student with scores 45 and 85, we predict an admission probability of %.3f.' \
	   % prediction(theta, 45, 85)

print 'Train Accuracy: %.3f'% accuracy(theta, exam_1, exam_2, y, M)