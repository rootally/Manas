#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import cv2
import sys
import matplotlib.pyplot as plt
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError 
import time

def ROI(img, vertices):
        
    mask = np.zeros_like(img)   
    
    mask_color = 255
          
    cv2.fillPoly(mask, vertices, mask_color)
    
    masked = cv2.bitwise_and(img, mask)
    return masked

def draw_lines(img, lines):

    # Calculate slopes and sizes
    lines = np.squeeze(lines)
    slopes = (lines[:,3]-lines[:,1]) / (lines[:,2] - lines[:,0])
    line_size = np.sqrt((lines[:,2] - lines[:,0])**2 + (lines[:,3]-lines[:,1])**2)

    # Get rid of outlier lines
    slope_threshold = 0.5
    lines = lines[np.abs(slopes)> slope_threshold]
    line_size = line_size[np.abs(slopes)> slope_threshold]
    slopes = slopes[np.abs(slopes)> slope_threshold]

    # Seperate positive and negative slopes, lines, and sizes
    left_slopes, right_slopes = slopes[slopes>0], slopes[slopes<0]
    left_lines, right_lines = lines[slopes>0,:], lines[slopes<0,:]
    left_lane_sizes, right_lane_sizes = line_size[slopes>0], line_size[slopes<0]

    # lanes sorted by size
    left_lane_sizes_sorted = np.argsort(left_lane_sizes)
    right_lane_sizes_sorted = np.argsort(right_lane_sizes)

    # Calculate average slope on the biggest 6 lines.
    left_slope_avg  = left_slopes[left_lane_sizes_sorted][-6::].mean()
    right_slope_avg = right_slopes[right_lane_sizes_sorted][-6::].mean()

    # find y intercept
    # b = y - m * x
    left_x_values, left_y_values = np.concatenate([left_lines[:,0], left_lines[:,2]]), np.concatenate([left_lines[:,1], left_lines[:,3]])
    right_x_values, right_y_values = np.concatenate([right_lines[:,0],right_lines[:,2]]), np.concatenate([right_lines[:,1], right_lines[:,3]])

    left_y_intercept = left_y_values - (left_slope_avg * left_x_values)
    right_y_intercept = right_y_values - (right_slope_avg * right_x_values)

    # find mean y-intercept based on n biggest lines
    left_y_intercept = left_y_intercept[left_lane_sizes_sorted][-6::]
    right_y_intercept = right_y_intercept[right_lane_sizes_sorted][-6::]
    left_y_intercept_avg, right_y_intercept_avg = np.mean(left_y_intercept), np.mean(right_y_intercept)

    imshape = img.shape
    img_floor = imshape[0]
    horizon_line = imshape[0]/1.5

    # calculate new points x = (y - b) / m

    # Right lane points
    max_right_x = (img_floor - right_y_intercept_avg) / right_slope_avg
    min_right_x = (horizon_line - right_y_intercept_avg) / right_slope_avg
    
    arr = [img_floor, right_slope_avg, max_right_x, right_y_intercept_avg ]
    


    # Left lane points
    min_left_x = (img_floor - left_y_intercept_avg) / left_slope_avg
    max_left_x = (horizon_line - left_y_intercept_avg) / left_slope_avg


    l1 = (int(min_left_x), int(img_floor))
    l2 = (int(max_left_x), int(horizon_line))
    # print('Right points l1 and l2,', l1, l2)
    cv2.line(img, l1, l2, [0, 255, 0], 8)

    r1 = (int(max_right_x), int(img_floor))
    r2 = (int(min_right_x), int(horizon_line))
    # print('Left points l1 and l2,', l1, l2)
    cv2.line(img, r1, r2, [255, 0, 0], 8)
    
    return arr




def callback(data):
    rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)


def image_callback(rosimg):

        pub = rospy.Publisher('coff_publisher', String, queue_size=100)
        rospy.init_node('slope_publisher', anonymous=True)

       print "image recieved"
           # Convert the image.
       try:
           bridge = CvBridge()
           img = bridge.imgmsg_to_cv2(rosimg, 'passthrough')
       except CvBridgeError, e:
           rospy.logwarn ('Exception converting background image from ROS to opencv:  %s' % e)
           img = np.zeros((320,240))
           
           #Display
       #cv2.namedWindow('image', cv2.WINDOW_NORMAL)
       #cv2.imshow('image', img)
       image = img
       gray = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
       blur = cv2.GaussianBlur(gray,(3,3), 0)
       edged = cv2.Canny(blur, 50,150)
       imshape = image.shape
       vertices = np.array([[(0, imshape[0]),
                                  (450, 320),
                                  (490, 320),
                                  (imshape[1], imshape[0]) ]],
                                  dtype=np.int32)

       img = ROI(edged, vertices )

       lines = cv2.HoughLinesP(img, 2, np.pi/180 , 5, np.array([]), 10, 20)
       final = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
       coff = draw_lines(final , lines)
       
       #publishing y=mx+c
       pub.publish(image_message)
       

       result = cv2.addWeighted(image, 0.9, final, 2, 3)

       #final result
       plt.imshow(result)
       cv2.waitKey(1000)
       cv2.destroyAllWindows()


def listener():

    counter=0
    rospy.init_node('listener', anonymous=True)
    bridge = CvBridge()
    lol = rospy.Subscriber('lane_image', Image , image_callback)
    #cv2.imshow(lol)
    
    rospy.spin()

if __name__ == '__main__':
    counter=0
    listener()
