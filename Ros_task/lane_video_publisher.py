#!/usr/bin/env python


import rospy
import cv2
import sys
import numpy as np
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError 
import time



def image_publisher():

    pub = rospy.Publisher('lane_image', Image, queue_size=100)
    rospy.init_node('lane_publisher', anonymous=True)
    rate = rospy.Rate(0.5)
    bridge = CvBridge()

    cap = cv2.VideoCapture('lane.mp4')

    while(cap.isOpened()):
        ret, frame = cap.read()
        cv2.imshow('frame',frame)   
        frame = np.uint8(frame)
        image_message = bridge.cv2_to_imgmsg(frame, encoding="passthrough")
        pub.publish(image_message)


        key = cv2.waitKey(1000)   

        if key == 27 or key == 1048603:
            break
    
    cap.release()
    cv2.destroyAllWindows()




if __name__ == '__main__':
    try:
        image_publisher()
    except rospy.ROSInterruptException:
        pass