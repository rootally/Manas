
from classes.shapedetector import ShapeDetector
from classes.colorlabeler import ColorLabeler
import argparse
import imutils
import cv2


ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,
	help="path to the input image")
args = vars(ap.parse_args())

image = cv2.imread(args["image"])
resized = imutils.resize(image, width=600)
ratio = image.shape[0] / float(resized.shape[0])


gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
lab = cv2.cvtColor(resized, cv2.COLOR_BGR2LAB)
##blurred = cv2.GaussianBlur(gray, (5, 5), 0)
edged = cv2.Canny(gray,0,130)
##thresh = cv2.threshold(edged, 200, 255, cv2.THRESH_BINARY)[1]
cv2.imshow('Canny',edged)
cv2.imshow('Gray', gray)


cnts = cv2.findContours(edged,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
##cv2.imshow('img', t)
cnts = cnts[0] if imutils.is_cv2() else cnts[1]
sd = ShapeDetector()
cl = ColorLabeler()
print(str(len(cnts)))


for c in cnts:
	
	M = cv2.moments(c)
	cX = int((M["m10"] / M["m00"]) * ratio)
	cY = int((M["m01"] / M["m00"]) * ratio)
	shape = sd.detect(c)
	color = cl.label(lab, c)
	
	c = c.astype("float")
	c *= ratio
	c = c.astype("int")
	text = "{} {}".format(color, shape)
	cv2.drawContours(image, [c], -1, (0, 0, 0), 2)
	cv2.putText(image, text, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX,1, (0, 0, 0), 2, cv2.LINE_AA)

	
	
cv2.imshow("Image", image)
cv2.waitKey(0)

