
import cv2

class ShapeDetector:
	def __init__(self):
		pass

	def detect(self, c):
		
		shape = "unidentified"
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.037* peri, True)
		
		if len(approx) == 3:
			shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w*h
			text = "Width {} Height {} Area {}".format(w,h,ar)
			print(text)
		
		elif len(approx) == 4:
			
			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w / float(h)

			shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
			ar = w*h
			text = "Width {} Height {} Area {}".format(w,h,ar)
			print(text)

		
		elif len(approx) == 5:
			shape = "pentagon"

		
		else:
			shape = "circle"
			(x,y),radius = cv2.minEnclosingCircle(c)
			radius = int(radius)
			print radius
		print(shape)
		
		return shape
