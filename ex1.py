from __future__ import division

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
sns.set_style("white")
sns.set_style("ticks")
sns.set_context("talk")

data = np.loadtxt('ex1/ex1data1.txt', delimiter=',')
x = data[:, 0] 
y = data[:, 1]

M = len(y)

plt.figure()
plt.scatter(x, y, c='r', marker='x', s=40)
plt.xlabel('Population of city in 10,000s')
plt.ylabel('Profit in $10,000s')
plt.xlim(4, 24)
plt.ylim(-5.2, 25.2)
plt.savefig('ex1a_univariate_plots/fig_1.png', dpi=300)

X = np.vstack((np.ones(M), x)).T
theta_0 = np.zeros(2)
def compute_cost(X, y, M, theta):
    return 1 / 2 / M * np.sum((theta.dot(X.T) - y)**2)

num_iters = 2000
alpha = 0.01

def gradient_descent(X, y, M, theta_0, alpha, num_iters):
    J_history = np.zeros(num_iters)
    theta = theta_0.copy()
    for i in xrange(num_iters):
        J_history[i] = compute_cost(X, y, M, theta)
        theta -= alpha / M * (theta.dot(X.T) - y).dot(X)
        
    return theta, J_history

theta, J_history = gradient_descent(X, y, M, theta_0, alpha, num_iters)
print "Theta found by gradient descent: %.3f, %.3f" % (theta[0], theta[1])
plt.figure()
plt.scatter(np.arange(num_iters), J_history, c='k', marker='o')
plt.xlabel('Steps')
plt.ylabel('Cost, ' + r'$J(\theta)$')
plt.xlim(-num_iters * 0.05, num_iters * 1.05)
plt.ylim(0, max(J_history) * 1.05)
plt.savefig('ex1a_univariate_plots/cost_vs_steps.png', dpi=300)
def prediction(theta, population):
    return theta[0] + population * theta[1]
plt.figure()
plt.scatter(x, y, c='r', marker='x', s=40, label='Training data')
x_range = np.linspace(min(x), max(x), 100)
plt.plot(x_range, prediction(theta, x_range), 'b-', label='Linear regression')
plt.xlabel('Population of city in 10,000s')
plt.ylabel('Profit in $10,000s')
plt.xlim(4, 24)
plt.ylim(-5, 25)
plt.legend(numpoints=1, loc=0)
plt.savefig('ex1a_univariate_plots/fig_2.png', dpi=300)

population = 3.5
print 'For population = %d, we predict a profit of %0.2f.' \
    % (population * 1e4, prediction(theta, population) * 1e4)

